﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CinemaWorldDTO
/// </summary>
namespace WebJet.Integration.Layer.Models
{
    public class ResponseDTO
    {
        public ResponseDTO()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string Title { get; set; }
        public string Year { get; set; }
        public string ID { get; set; }
        public string Type { get; set; }
        public string Poster { get; set; }

    }
}