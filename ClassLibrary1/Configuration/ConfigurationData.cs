﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace WebJet.Integration.Layer.Configuration
{
    public static class ConfigurationData
    {
        public static string GetValuebyKey(string key)
        {
            string val = "";
            try
            {
               val = ConfigurationManager.AppSettings[key];
            }
            catch(Exception ex)
            {

            }
            finally
            {

            }
            return val;
        }
    }
}
