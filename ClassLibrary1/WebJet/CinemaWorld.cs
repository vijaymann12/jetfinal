﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebJet.Integration.Layer.Models;
using RestSharp;
using static WebJet.Integration.Layer.Configuration.ConfigurationData;
using Newtonsoft.Json;

/// <summary>
/// Summary description for CinemaWorldApi
/// </summary>
namespace WebJet.Integration.Layer.WebJetApis
{
    public static class CinemaWorld
    {
        private const string APIKey = "Token";
        private const string APIUrlMovies = "/api/cinemaworld/movies";
        private const string APIUrlMovie = "/api/cinemaworld/movie";
        public static Dictionary<string, List<ResponseDTO>> GetCinemaMoviesFromSource()
        {
            var client = new RestClient(GetValuebyKey("ApiAccessUrl"));
            var request = new RestRequest( APIUrlMovies , Method.GET);
            request = AddHeaders(request);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            IRestResponse<ResponseDTO> response2 = client.Execute<ResponseDTO>(request);

            return JsonConvert.DeserializeObject<Dictionary<string, List<ResponseDTO>>>(response2.Content);
        }

        public static  ResponseDetailDTO GetCinemaMoviesFromSourcebyID(string input)
        {
            var client = new RestClient(GetValuebyKey("ApiAccessUrl"));
            var request = new RestRequest(APIUrlMovie + "/" + input, Method.GET);
            request = AddHeaders(request);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            IRestResponse<ResponseDTO> response2 = client.Execute<ResponseDTO>(request);

            return JsonConvert.DeserializeObject<ResponseDetailDTO>(response2.Content);
        }

        public static RestRequest AddHeaders(RestRequest request)
        {
            request.AddHeader("x-access-token",GetValuebyKey(APIKey));
            return request;
        }

    }
}
