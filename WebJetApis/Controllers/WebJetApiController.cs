﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static WebJet.Integration.Layer.WebJetApis.CinemaWorld;
using static WebJet.Integration.Layer.WebJetApis.FilmWorld;
using WebJet.Integration.Layer.Models;
using System.Web.Http.Results;

namespace WebJetApis.Controllers
{
 
  public class WebJetApiController : ApiController
    {
        [HttpGet]
        public List<ResponseDTO> GetCinemaMovies()
        {
            var result = new List<ResponseDTO>();
            try
            {
                result = GetCinemaMoviesFromSource()?.First().Value;

            }
            catch (Exception ex) { }
            finally {
                
            }
            return result;
        }

       
        [HttpGet]
        public ResponseDetailDTO GetCinemaMoviebyID(string ID)
        {
            var result = new ResponseDetailDTO();
            try
            {
                result = GetCinemaMoviesFromSourcebyID(ID);
            }
            catch (Exception ex) { }
            finally { }
            return result;
        }

      
        [HttpGet]
        public List<ResponseDTO> GetFilmWorldMovies()
        {
            var result = new List<ResponseDTO>();
            try
            {
                result = GetFilmWorldMoviesFromSource()?.First().Value;
            }
            catch (Exception ex) { }
            finally { }
            return result;
        }

       
        [HttpGet]
        public ResponseDetailDTO GetFilmWorldMoviebyID(string ID)
        {
            var result = new ResponseDetailDTO();
            try
            {
                result = GetFilmWorldMoviesFromSourcebyID(ID);
            }
            catch (Exception ex) { }
            finally { }
            return result;
        }
     }
  }

