﻿(function () {
    'use strict';
    angular.module(
        'Movies', [
        'Movies.Directives',
        'Movies.Factories',
        'Movies.Services',
        'Movies.Filters']);
    // Namespaces
    angular.module('Movies.Directives', []);
    angular.module('Movies.Factories', []);
    angular.module('Movies.Services', []);
    angular.module('Movies.Filters', []);
})();

