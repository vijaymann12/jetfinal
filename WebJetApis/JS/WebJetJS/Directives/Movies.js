﻿"use strict";
/* Directives */
angular.module('Movies.Directives')
.directive('wjMovies', ['$http', 'API', function ($http, API) {
    return {
        scope: true,
        restrict: 'A',
        controller: MoviesController,
        controllerAs: 'ffc',
        bindToController: true,
        link: function (scope, element, attrs, filter, http) {
          
        }
    };
    MoviesController.$inject = ['$scope', '$http',  '$rootScope','$filter'];
    function MoviesController($scope, $http, $rootScope,$filter) {
        var ftd = this;
       
        $scope.init = function () {
            console.log("Test wjMovies");
            $scope.getCinemaMovies();
            $scope.getFilmWorldMovies();
        }

        $scope.compareMoviePrices = function () {
            console.log($scope.movie);
            console.log($scope.moviefilm);

            $scope.Result = null;
            $scope.FilmResult = null;

            if ($scope.movie) {
                $scope.getCinemaMoviesbyID(JSON.parse($scope.movie).ID);
                $scope.getFilmWorldMoviesbyID('fw'+JSON.parse($scope.movie).ID.substring(2));
                
            }

            if ($scope.moviefilm) {
                $scope.getCinemaMoviesbyID('cw' + JSON.parse($scope.moviefilm).ID.substring(2));
                $scope.getFilmWorldMoviesbyID(JSON.parse($scope.moviefilm).ID);
            }
        }


        $scope.getCinemaMovies = function () {
            $http({
                method: 'GET', url: $filter('formatArgs')(Movies.EnviornmentPath.Url + Movies.API.LookUps.GetAllCinemaWorldMovies,
               [])
            })
            .then(function successCallback(data) {
               
                $scope.MoviesAll = data.data;
                console.log(data);

            }, function errorCallback(data) {
               
            });



        }


        $scope.getFilmWorldMovies = function () {
            $http({
                method: 'GET', url: $filter('formatArgs')(Movies.EnviornmentPath.Url + Movies.API.LookUps.GetAllFilmWorldMovies,
               [])
            })
            .then(function successCallback(data) {

                $scope.FilmWorldMovies = data.data;
                console.log(data);

            }, function errorCallback(data) {

            });
        }
       
        

        $scope.getCinemaMoviesbyID = function (ID) {
            $http({
                method: 'GET', url: $filter('formatArgs')(Movies.EnviornmentPath.Url + Movies.API.LookUps.GetCinemaWorldMoviesByID,
               [ID])
            })
            .then(function successCallback(data) {
                if (data.data) {
                    $scope.Result = data.data.Title + " has " + data.data.Price + " in Cinema Movies";
                    $scope.CinemaPrice = data.data.Price;
                    console.log(data);
                }
                else
                    $scope.Result = null;
            }, function errorCallback(data) {

            });



        }


        $scope.getFilmWorldMoviesbyID = function (ID) {
            $http({
                method: 'GET', url: $filter('formatArgs')(Movies.EnviornmentPath.Url + Movies.API.LookUps.GetAllFilmWorldMoviesByID,
               [ID])
            })
            .then(function successCallback(data) {
                if (data.data) {
                    $scope.FilmResult = data.data.Title + " has " + data.data.Price + " in Film World Movies";
                    $scope.FilmPrice = data.data.Price;
                    console.log(data);
                }
                else
                    $scope.FilmResult = null;
            }, function errorCallback(data) {

            });
        }

        $scope.hightlightLessPrice = function(type)
        {
            (parseInt($scope.CinemaPrice) < parseInt($scope.FilmPrice) || $scope.CinemaPrice == null || $scope.FilmPrice == null) ? $scope.Cinema = true : $scope.Cinema = false;

            if (type == '1') {
                  return (parseInt($scope.CinemaPrice)< parseInt($scope.FilmPrice) || $scope.CinemaPrice==null || $scope.FilmPrice ==null) ? 'text-success' : 'text-danger';
            }
            else
            {
                return (parseInt($scope.CinemaPrice) > parseInt($scope.FilmPrice) || $scope.CinemaPrice == null || $scope.FilmPrice == null) ? 'text-success' : 'text-danger';
            }
        }

        $scope.clearOtherDropDown = function (type) {
            if (type) $scope.movie = null; else $scope.moviefilm = null;
        }


    }
}]);
