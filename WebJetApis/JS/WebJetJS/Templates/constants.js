﻿

var Movies = {
    API: {
        LookUps: {
            GetAllCinemaWorldMovies: '/api/WebJetapi/GetCinemaMovies',
            GetCinemaWorldMoviesByID: '/api/WebJetapi/GetCinemaMoviebyID/{0}',
            GetAllFilmWorldMovies: '/api/WebJetapi/GetFilmWorldMovies',
            GetAllFilmWorldMoviesByID: '/api/WebJetapi/GetFilmWorldMoviebyID/{0}',
        }
    },
    EnviornmentPath :
        {
            Url: 'http://localhost/webjet/',
        }
}
