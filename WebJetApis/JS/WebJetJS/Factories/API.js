﻿"use strict";
angular.module('Movies.Factories')
.factory('API', ['$http', '$q', function ($http, $q) { // Query API and return JSON.
    return {
        get: function (url) {
            var deferred = $q.defer();

            $http({ method: 'GET', url: url }).
            success(function (data) {
                if (data.ErrorMessage) {
                    deferred.reject(data, 200);
                } else {
                    deferred.resolve(data);
                }
            }).
            error(function (data, status) {
                var myresult = { reason: data, status: status, ErrorMessage: status.toString() };
                deferred.reject(myresult);
            });

            return deferred.promise;
        },
        post: function (url, data) {
            var deferred = $q.defer();

            $http({ method: 'POST', url: url, data: data }).
            success(function (data) {
                if (data.ErrorMessage) {
                    deferred.reject(data, 200);
                } else {
                    deferred.resolve(data);
                }
            }).
            error(function (data, status) {
                var myresult = { reason: data, status: status, ErrorMessage: status.toString() };
                deferred.reject(myresult);
            });

            return deferred.promise;
        }
    };
}]);