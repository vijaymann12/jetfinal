﻿"use strict";

angular.module('Movies.Filters', [])
.filter('formatArgs', [function () { // pass url and array of arguments.
    return function (url, args) {

        // Loop through arguments and replace url value.
        angular.forEach(args, function (value, key) {
            url = url.replace("{" + key + "}", value);
        });

        return url;
    };
}])