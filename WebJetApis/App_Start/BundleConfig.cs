﻿using System.Web;
using System.Web.Optimization;

namespace WebJetApis
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/JS").Include(
                        "~/JS/app.js").Include("~/JS/WebJetJS/Factories/*.js"
                         ).Include("~/JS/WebJetJS/directives/*.js").Include
                         ("~/JS/WebJetJS/Templates/*.js").Include
                         ("~/JS/WebJetJS/Filters/*.js")                
                        );

            bundles.Add(new StyleBundle("~/bundles/styles").Include(
                      "~/Content/*.css").Include("~/Content/*.map"));

        }
    }
}
