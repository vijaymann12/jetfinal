﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;

using Owin;


namespace WebJetApis
{
    public partial class Startup
    {
       // public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            //// Configure the db context and user manager to use a single instance per request
            //app.CreatePerOwinContext(ApplicationDbContext.Create);
            //app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);


            //    //// Uncomment the following lines to enable logging in with third party login providers
            //    //app.UseMicrosoftAccountAuthentication(
            //    //    clientId: "",
            //    //    clientSecret: "");

            //    //app.UseTwitterAuthentication(
            //    //    consumerKey: "",
            //    //    consumerSecret: "");

            //    //app.UseFacebookAuthentication(
            //    //    appId: "",
            //    //    appSecret: "");

            //    //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //    //{
            //    //    ClientId = "",
            //    //    ClientSecret = ""
            //    //});
            }
        }
    }
